https://tawright28.gitlab.io/towerofhanoi

Development Plan:
1. Create user interface
    a. create HTML elements 
    b. correspond elements with CSS
    c. create layout
2. Create logic/algorithms
    a. assign click handlers to HTML elements
    b. use functions to determine placement of disks
    c. implement error detection
3. Check winning outcome
    a. use if statement to display winning phrase once all disks were in correct places

Members:
Tuan Wright
Kevin Grove