const diskAreaEnd = document.querySelector("#diskAreaEnd")
const diskAreaOffset = document.querySelector("#diskAreaOffset")
const diskAreaStart = document.querySelector("#diskAreaStart")
const winningDiv = document.querySelector("#winningDiv")

const selectedAreaStart = function (event) {
    event.stopPropagation()
    let topDisk = event.target.parentNode.lastElementChild
    let topDiskClone = topDisk.cloneNode(true)
    
    if (event.target.className == 'diskarea') {
        return
    }
    
    if (diskAreaOffset.lastElementChild == null || diskAreaOffset.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaOffset.appendChild(topDiskClone)

    } else if (diskAreaEnd.lastElementChild == null || diskAreaEnd.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaEnd.appendChild(topDiskClone)

    }

    if (diskAreaEnd.childElementCount === 4) {
        winningDiv.innerHTML = "YOU'VE WON!"
    }

    console.log(diskAreaEnd.childElementCount)
}

const selectedAreaOffset = function (event) {
    event.stopPropagation()
    let topDisk = event.target.parentNode.lastElementChild
    let topDiskClone = topDisk.cloneNode(true)
    
    if (event.target.className == 'diskarea') {
        return
    }
    
    if (diskAreaEnd.lastElementChild == null || diskAreaEnd.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaEnd.appendChild(topDiskClone)

    } else if (diskAreaStart.lastElementChild == null || diskAreaStart.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaStart.appendChild(topDiskClone)

    }

    if (diskAreaEnd.childElementCount === 4) {
        winningDiv.innerHTML = "YOU'VE WON!"
    }
    
    console.log(diskAreaEnd.childElementCount)
}

const selectedAreaEnd = function (event) {
    event.stopPropagation()
    let topDisk = event.target.parentNode.lastElementChild
    let topDiskClone = topDisk.cloneNode(true)

    if (diskAreaEnd.childElementCount === 4) {
        return
    }
    
    if (event.target.className == 'diskarea') {
        return
    }
    
    if (diskAreaStart.lastElementChild == null || diskAreaStart.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaStart.appendChild(topDiskClone)

    } else if (diskAreaOffset.lastElementChild == null || diskAreaOffset.lastElementChild.dataset.width > topDisk.dataset.width) {
        topDisk.remove()
        diskAreaOffset.appendChild(topDiskClone)

    }
    
    if (diskAreaEnd.childElementCount === 4) {
        winningDiv.innerHTML = "YOU'VE WON!"
    }
    
    console.log(diskAreaEnd.childElementCount)
}


diskAreaStart.addEventListener('click', selectedAreaStart)

diskAreaOffset.addEventListener('click', selectedAreaOffset)

diskAreaEnd.addEventListener('click', selectedAreaEnd)
